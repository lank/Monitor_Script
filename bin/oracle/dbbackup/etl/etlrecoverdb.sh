#!/bin/sh
##############
#
#
#备份和还原文件的数据库目录结构都要一致
#还原后,手工添加参数文件
#sh rec.sh /bi/dbbackup/20131220
#############

if [[ -n $1 ]];then
	bakdir=$1
else
	echo "input parameter."
	exit 0
fi

 
pfile=$bakdir/pfile.ora
tempfile="/tmp/temp01.dbf"
 
#step 1 get spfile
cnt=`ls $bakdir |grep spfile |wc -l`
spfile=`ls $bakdir |grep spfile `
strings  ${bakdir}/$spfile|grep "*" >$pfile


#step 2 db to nomount
export ORACLE_SID=etl
sqlplus "/ as sysdba"<<EOF
startup nomount pfile='$pfile'
quit
EOF

#step3 recover controlfile ,restore database, recover database  
#get controfile
cnt=`ls $bakdir |grep ctl |wc -l`
ctlfile=`ls $bakdir |grep ctl `
echo "ctlfile is ${bakdir}${ctlfile}"

CMD_STR="
ORACLE_HOME=$ORACLE_HOME
export ORACLE_HOME
ORACLE_SID=$ORACLE_SID
export ORACLE_SID
rman  target  /  << EOF
RUN {
allocate channel d1 type disk;
restore controlfile   from  '${bakdir}/${ctlfile}';
alter database mount;
restore database ;
recover database;
release channel d1;
}
EOF
"
/bin/sh -c "$CMD_STR"   

#step 4 open database
sqlplus "/ as sysdba" <<EOF
alter database open resetlogs;
quit
EOF


#other process
sqlplus "/ as sysdba" <<EOF
create spfile from pfile='$pfile';
startup force;
quit
EOF

#

exit 0
 
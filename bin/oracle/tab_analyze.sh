#!/bin/sh
host_name=`hostname`
ostype=`uname 2>/dev/null`
case $ostype in
  linux*|LINUX*|Linux*)
    . $HOME/.bash_profile
        ;;
        AIX*|aix*)
    . $HOME/.profile
        ;;
        HP*|hp*)
        . $HOME/.profile
        ;;
  *)
   . $HOME/.profile
    ;;
esac
date
sqlplus /nolog <<EOF
conn /as sysdba ;
set serveroutput on
declare
   -----select OVER THE Change RATE TABLES---------------
   cursor overchangerate is
 select a.table_owner, a.table_name, a.inserts,a.updates,a.deletes ,b.num_rows
                from dba_tab_modifications a, dba_tables b
               where a.table_name = b.table_name
                 and table_owner not in
                     ('SYS', 'SYSTEM', 'SYSMAN', 'DMSYS', 'OLAPSYS', 'XDB',
                      'EXFSYS', 'CTXSYS', 'WMSYS', 'DBSNMP', 'ORDSYS',
                      'OUTLN', 'TSMSYS', 'MDSYS','XXB_BACKUP','XXB_TEST')
                 and inserts > 0 and partitioned='NO' and a.inserts/decode(b.num_rows,0,1,b.num_rows)>=0.1
                 or a.table_name = b.table_name
                 and table_owner not in
                     ('SYS', 'SYSTEM', 'SYSMAN', 'DMSYS', 'OLAPSYS', 'XDB',
                      'EXFSYS', 'CTXSYS', 'WMSYS', 'DBSNMP', 'ORDSYS',
                      'OUTLN', 'TSMSYS', 'MDSYS','XXB_BACKUP','XXB_TEST')
                 and updates > 0 and partitioned='NO' and a.updates/decode(b.num_rows,0,1,b.num_rows)>=0.1 or
                 a.table_name = b.table_name
                 and table_owner not in
                     ('SYS', 'SYSTEM', 'SYSMAN', 'DMSYS', 'OLAPSYS', 'XDB',
                      'EXFSYS', 'CTXSYS', 'WMSYS', 'DBSNMP', 'ORDSYS',
                      'OUTLN', 'TSMSYS', 'MDSYS','XXB_BACKUP','XXB_TEST')
                 and deletes > 0 and partitioned='NO' and a.deletes/decode(b.num_rows,0,1,b.num_rows)>=0.1 ;

  begin
    dbms_output.enable(1000000);
    ----flush the monitorring information into the dba_tab_modifications
    DBMS_STATS.FLUSH_DATABASE_MONITORING_INFO;

    dbms_output.put_line('Unalalyzed tables:');
    ----display the  information-------------------
    dbms_output.put_line('- - - - - - - - - - - - - - - - - - - - -  - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -' );
    dbms_output.put_line('Over the Change_Rate 10%:');
    for v_topinsert in overchangerate loop
      dbms_output.put_line(v_topinsert.table_owner || '.' || v_topinsert.table_name || ' once has ' || v_topinsert.num_rows || ' rows, ' ||
                           'till now inserted ' || v_topinsert.inserts || ' rows, updated  ' || v_topinsert.updates || ' rows, deleted ' || v_topinsert.deletes ||   
                           ' rows. consider gathering statistics');
    end loop;
     dbms_output.put_line('- - - - - - - - - - - - - - - - - - -  - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -');  
 --analyze normal table 10
   dbms_output.put_line('Table dml pct 10' );
   for r1 in overchangerate loop
    begin
    DBMS_STATS.GATHER_TABLE_STATS(ownname          => r1.table_owner,
                                  tabname          => r1.table_name,
                                  estimate_percent => 1,
                                  method_opt       => 'FOR ALL COLUMNS SIZE 1',
                                  no_invalidate    => false,
                                  force            => true,
                                  cascade          => true,
                                  degree           => 4);
    end;
   dbms_output.put_line('Normal Table modify 10%:'||r1.table_owner||'.'||r1.table_name|| ' ' );
   end loop;
end;
/


--second analyze
declare
  cursor c1 is
    select a.owner, a.segment_name
  from dba_segments a, dba_tables b
 where a.segment_type = 'TABLE'
   and a.segment_name not like 'BIN%'
   and a.owner = b.owner
   and a.owner = 'XXB'
   and a.segment_name = b.table_name
   and (b.last_analyzed is null or
       (a.blocks - b.blocks) * 100 /
       nvl(decode(b.blocks, 0, 1, b.blocks), 1) > 10 and a.blocks > 1000)
union
SELECT a.owner, a.object_name
  FROM dba_objects a, dba_Tables b
 WHERE object_type like 'TABLE%'
   and a.owner = b.owner
   and b.partitioned = 'NO'
   and a.object_name = b.table_name
   AND object_name not like 'BIN%'
   AND a.last_ddl_time > b.last_analyzed
   and a.owner = 'XXB'
union
SELECT a.owner, a.object_name
  FROM dba_objects a, dba_Tables b
 WHERE object_type like 'TABLE%'
   and a.owner = b.owner
   and b.partitioned = 'NO'
   and a.object_name = b.table_name
   AND object_name not like 'BIN%'
   AND b.last_analyzed<sysdate-90
   and a.owner = 'XXB'
union
select distinct a.owner, a.segment_name
  from dba_segments a, dba_tab_partitions b
 where a.segment_type = 'TABLE PARTITION'
   and a.segment_name not like 'BIN%'
   and a.owner = b.table_owner
   and a.owner = 'XXB'
   and a.segment_name = b.table_name
   and a.partition_name = b.partition_name
   and (b.last_analyzed is null or
       (a.blocks - b.blocks) * 100 /
       nvl(decode(b.blocks, 0, 1, b.blocks), 1) > 10 and a.blocks > 1000 )
   ;
begin
  for r3 in c1 loop
    begin
     DBMS_STATS.GATHER_TABLE_STATS(ownname          => r3.owner,
                                   tabname          => r3.segment_name,
                                   estimate_percent => 1,
                                   method_opt       => 'FOR ALL COLUMNS SIZE 1',
                                   no_invalidate    => false,
                                   force            => true,
                                   cascade          => true,
                                   degree           => 5);
         dbms_output.put_line('Table not analyze and part and normal table:'||r3.owner||r3.segment_name|| ' ' );
    end;
  end loop;
end;
/

--partation table
BEGIN
   DBMS_STATS.GATHER_TABLE_STATS(ownname => 'XXB',
                                 tabname => 'DRP_XIAOS_ZHIW',
                                 estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                 method_opt => 'for all columns size repeat',
                                 degree => DBMS_STATS.AUTO_DEGREE,
                                 granularity => 'ALL',
                                 cascade=>TRUE
                                 );
END;
/

BEGIN
   DBMS_STATS.GATHER_TABLE_STATS(ownname => 'XXB',
                                 tabname => 'BUH_PUHMSCL_MUBKC',
                                 estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                 method_opt => 'for all columns size repeat',
                                 degree => DBMS_STATS.AUTO_DEGREE,
                                 granularity => 'ALL',
                                 cascade=>TRUE
                                 );
END;
/


BEGIN
   DBMS_STATS.GATHER_TABLE_STATS(ownname => 'XXB',
                                 tabname => 'DRP_XIAOS\$DRP',
                                 estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                 method_opt => 'for all columns size repeat',
                                 degree => DBMS_STATS.AUTO_DEGREE,
                                 granularity => 'ALL',
                                 cascade=>TRUE
                                 );
END;
/


BEGIN
   DBMS_STATS.GATHER_TABLE_STATS(ownname => 'XXB',
                                 tabname => 'DRP_XIAOS',
                                 estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                 method_opt => 'for all columns size repeat',
                                 degree => DBMS_STATS.AUTO_DEGREE,
                                 granularity => 'ALL',
                                 cascade=>TRUE
                                 );
END;
/


BEGIN
   DBMS_STATS.GATHER_TABLE_STATS(ownname => 'XXB',
                                 tabname => 'DRP_XIAOS_MAIC_HUIZ',
                                 estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                 method_opt => 'for all columns size repeat',
                                 degree => DBMS_STATS.AUTO_DEGREE,
                                 granularity => 'ALL',
                                 cascade=>TRUE
                                 );
END;
/

BEGIN
   DBMS_STATS.GATHER_TABLE_STATS(ownname => 'XXB',
                                 tabname => 'FACT_POSLSH',
                                 estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                 method_opt => 'for all columns size repeat',
                                 degree => DBMS_STATS.AUTO_DEGREE,
                                 granularity => 'ALL',
                                 cascade=>TRUE
                                 );
END;
/


BEGIN
   DBMS_STATS.GATHER_TABLE_STATS(ownname => 'XXB',
                                 tabname => 'DRP_XIAOS_HUOH',
                                 estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                 method_opt => 'for all columns size repeat',
                                 degree => DBMS_STATS.AUTO_DEGREE,
                                 granularity => 'ALL',
                                 cascade=>TRUE
                                 );
END;
/


BEGIN
   DBMS_STATS.GATHER_TABLE_STATS(ownname => 'XXB',
                                 tabname => 'FACT_POSLSH\$BI',
                                 estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                 method_opt => 'for all columns size repeat',
                                 degree => DBMS_STATS.AUTO_DEGREE,
                                 granularity => 'ALL',
                                 cascade=>TRUE
                                 );
END;
/


exit;
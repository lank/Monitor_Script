#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#sh run.sh $sid
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#. ${scriptsdir}/lib/sysf/reclog.fun
if [[ -n $1 ]];then
  export ORACLE_SID=$1
fi
#env
spoolfile=$scriptsdir/tmp/ora.db.tbs.$$.out
lockurl=${scriptsdir}/locks/ora.db.tbs.lck
if [[ `islock $lockurl` -eq 1 ]];then
    #reclog "ORADB.TBS" "ERROR:file is locked."
    exit 1
fi
lock $lockurl $$
######Main############################################
#monitor db tbs usage to table:mon_tbs_usage;
sqlplus "/ as sysdba">/dev/null<<EOF
set embedded off
ttitle off
btitle off
set pagesize 0
set wrap off
set linesize 2000
set trimspool on
set newpage 0
set space 0
set trunc off
set arraysize 1
set heading off
set feedback off
set verify off
set termout off
set underline '-'
set pause off
clear breaks
clear columns
col msg for a120
spool $spoolfile
select 'DATA'||','||
       a.tbs_name||','||
       a.sum_space||','||
       a.sum_blocks||','||
       a.used_space||','||
       a.used_rate||','||
       a.free_space||','||
       c.max_free_space msg
  from (select d.tablespace_name tbs_name,
               space sum_space,
               blocks sum_blocks,
               space - nvl(free_space, 0) used_space,
               round((1 - nvl(free_space, 0) / space) * 100, 0) used_rate,
               free_space free_space
          from (select tablespace_name,
                       round(sum(bytes) / (1024 * 1024), 2) space,
                       sum(blocks) blocks
                  from dba_data_files
                 group by tablespace_name) d,
               (select tablespace_name,
                       round(sum(bytes) / (1024 * 1024), 2) free_space
                  from dba_free_space
                 group by tablespace_name) f
         where d.tablespace_name = f.tablespace_name(+)) a,
       (select tablespace_name tbs_name
             --  round(sum(free) * 100 / sum(bytes), 0) wast_pct
          from (select b.file_id file_id,
                       b.tablespace_name tablespace_name,
                       b.bytes bytes,
                       (b.bytes - sum(nvl(a.bytes, 0))) used,
                       sum(nvl(a.bytes, 0)) free,
                       sum(nvl(a.bytes, 0)) / (b.bytes) * 100 percent
                  from dba_free_space a, dba_data_files b
                 where a.file_id = b.file_id
                 group by b.tablespace_name, b.file_id, b.bytes
                 order by b.file_id)
         group by tablespace_name
         order by sum(free) * 100 / sum(bytes)) b,
       (select tablespace_name tbs_name,
               round(max(bytes) / 1024 / 1024, 0) max_free_space
          from dba_free_space
         group by tablespace_name) c
 where a.tbs_name = b.tbs_name
   and b.tbs_name = c.tbs_name;
spool off
quit
EOF
##format file
grep "DATA" $spoolfile |grep -v "select" |awk -F ',' '{print $2,$3,$4,$5,$6,$7,$8}'|
while read f2 f3 f4 f5 f6 f7 f8
do
  if [[ $dataformat -eq 1 ]];then
    echo "insert into mn_oradb_tbs values('$rid.db.{$ORACLE_SID}','$ORACLE_SID','$f2',$f3,$f4,$f5,$f6,$f7,$f8,'$(date "+%Y%m%d:%H%M%S")',to_date(to_char(sysdate,'yyyy-mm-dd hh24:mi'),'yyyy-mm-dd hh24:mi'))">>$dataformaturl
    echo "--NEXT">>$dataformaturl
  fi
done
######Exit############################################
rm -f $spoolfile >/dev/null
unlock $lockurl
exit 0
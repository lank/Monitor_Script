begin
dbms_stats.gather_table_stats(ownname=>'ECOLOGY1124',
tabname=>'DOCDETAIL',
estimate_percent =>'dbms_stats.auto_sample_size',
method_opt =>'FOR ALL COLUMNS SIZE 1',
degree=>1,
cascade=>true,
no_invalidate=>false);
end;
/
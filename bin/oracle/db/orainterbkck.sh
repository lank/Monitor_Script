#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#sh run.sh $sid
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
if [[ -n $1 ]];then
  export ORACLE_SID=$1
fi
#env
spoolfile=$scriptsdir/tmp/ora.db.rmanbkck.$$.out
lockurl=${scriptsdir}/locks/ora.db.rmanbkck.lck
if [[ `islock $lockurl` -eq 1 ]];then
    #reclog "ORADB.RMBKCK" "ERROR:file is locked."
    exit 1
fi
lock $lockurl $$
######Main############################################
sqlplus "/ as sysdba">/dev/null<<EOF
set embedded off
ttitle off
btitle off
set pagesize 0
set wrap off
set linesize 2000
set trimspool on
set newpage 0
set space 0
set trunc off
set arraysize 1
set heading off
set feedback off
set verify off
set termout off
set underline '-'
set pause off
clear breaks
clear columns
col message for a90
spool $spoolfile
  select 'DATA' || ',' || stime || ',' || etime || ',' || elapsedtime || ',' ||
         sizeg || ',' || mark msg
    from (select to_char(decode(stime, null, sysdate, stime),
                         'yyyy-mm-dd hh24:mi:ss') stime,
                 to_char(decode(etime, null, sysdate, etime),
                         'yyyy-mm-dd hh24:mi:ss') etime,
                 to_char(decode(elapsedtime, null, 0, elapsedtime),
                         'fm999999990.999999999') elapsedtime,
                 to_char(decode(sizeg, null, 0, sizeg),
                         'fm999999990.999999999') sizeg,
                 mark
            from (select min(start_time) stime,
                         max(completion_time) etime,
                         trunc(sum(elapsed_seconds) / 60, 2) elapsedtime,
                         trunc(sum(bytes) / 1024 / 1024 / 1024, 2) sizeg,
                         decode(min(start_time), null, 'failed', 'success') mark
                    from (select a.recid,
                                 a.backup_type,
                                 a.start_time,
                                 a.completion_time,
                                 a.ELAPSED_SECONDS,
                                 a.CONTROLFILE_INCLUDED,
                                 b.bytes,
                                 b.tag,
                                 b.handle
                            from V\$BACKUP_SET a, V\$BACKUP_PIECE b
                           where a.recid = b.recid
                             and a.start_time >
                                 to_date(to_char(sysdate, 'yyyy-mm-dd'),
                                         'yyyy-mm-dd'))));
spool off
quit
EOF
##format sqlfile
f2=`grep "DATA" $spoolfile|grep -v "select"|awk -F ',' '{print $2}'`
f3=`grep "DATA" $spoolfile|grep -v "select"|awk -F ',' '{print $3}'`
f4=`grep "DATA" $spoolfile|grep -v "select"|awk -F ',' '{print $4}'`
f5=`grep "DATA" $spoolfile|grep -v "select"|awk -F ',' '{print $5}'`
f6=`grep "DATA" $spoolfile|grep -v "select"|awk -F ',' '{print $6}'`

if [[ $dataformat -eq 1 ]];then
	echo "insert into mn_backup values('$rid.db.{$ORACLE_SID}','ORAINTER','SQL','ORADB',to_date('$f2','yyyy-mm-dd hh24:mi:ss'),to_date('$f3','yyyy-mm-dd hh24:mi:ss'),$f4,$f5,'$f6','mark',sysdate)"
	echo "insert into mn_backup values('$rid.db.{$ORACLE_SID}','ORAINTER','SQL','ORADB',to_date('$f2','yyyy-mm-dd hh24:mi:ss'),to_date('$f3','yyyy-mm-dd hh24:mi:ss'),$f4,$f5,'$f6','mark',sysdate)">>$dataformaturl
	echo "--NEXT">>$dataformaturl
fi

######Exit############################################
rm -f $spoolfile>/dev/null
unlock $lockurl
exit 0
#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#sh run.sh $sid
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#. ${scriptsdir}/lib/sysf/reclog.fun
if [[ -n $1 ]];then
	export ORACLE_SID=$1
fi
#env
spoolfile=$scriptsdir/tmp/ora.db.onoff.$$.out
>$spoolfile
lockurl=${scriptsdir}/locks/ora.db.onoff.lck
if [[ `islock $lockurl` -eq 1 ]];then
	#reclog "ORADB.ONOFF" "ERROR:file is locked."
    exit 1
fi
lock $lockurl $$
######Main############################################
sqlplus "/ as sysdba">/dev/null<<EOF
set embedded off
ttitle off
btitle off
set pagesize 0
set wrap off
set linesize 2000
set trimspool on
set newpage 0
set space 0
set trunc off
set arraysize 1
set heading off
set feedback off
set verify off
set termout off
set underline '-'
set pause off
clear breaks
clear columns
col msg for a90
spool $spoolfile
select 'DATA,'||open_mode msg from v\$database;
spool off
quit
EOF
##format sqlfile
f2=`grep "DATA" $spoolfile|grep -v "select"|grep READ|grep WRITE|awk -F ',' '{print $2}'`
if [[ $dataformat -eq 1 ]];then
	if [[ $f2 = "READ WRITE" && $(ps -ef|grep tnslsnr|grep -v grep|wc -l) -gt 0 ]];then
		echo "insert into mn_node_heartbeat values('$rid.db.{$ORACLE_SID}','active',sysdate)">>$dataformaturl
	else
		echo "insert into mn_node_heartbeat values('$rid.db.{$ORACLE_SID}','inactive',sysdate)">>$dataformaturl
	fi
fi
echo "--NEXT">>$dataformaturl
######Exit############################################
rm -f $spoolfile>/dev/null
unlock $lockurl
exit 0

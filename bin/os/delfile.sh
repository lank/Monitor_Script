#!/bin/sh
#6 6 * * *  sh /mn/bin/os/delfile.sh /home/oracle/u01/app/archivelog 2 dbf 
dir=$1
day=$2
type=$3
if [[ -n $dir && -n $day && -n $type && -d $dir && $day -ge 0 && $day -le 90 && $dir != "." && $dir != "/" ]];then
	cd $dir || cd /tmp
	find . -mtime +$day -name "*"$type -exec rm -f {} \;
fi
exit 0

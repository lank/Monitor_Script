#!/bin/sh
#mn.cnf:dataformat,dataformaturl
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#. ${scriptsdir}/lib/sysf/reclog.fun
#env
lockurl=${scriptsdir}/locks/nginx.lck
datafile=${scriptsdir}/data/nginx.dat
if [ `islock $lockurl` -eq 1 ];then
	#reclog  "OS.NGINX" "ERROR:file is locked."
	exit 1
fi
lock $lockurl $$
######Main############################################
nginx_name="";
if [[ -n $1 ]];then
 	nginx_name=$1;
fi
HOST=$2
PORT=$3
function active {
  /usr/bin/curl "http://$HOST:$PORT/nginx-status" 2>/dev/null| grep 'Active' | awk '{print $NF}'
  }
function reading {
  /usr/bin/curl "http://$HOST:$PORT/nginx-status" 2>/dev/null| grep 'Reading' | awk '{print $2}'
  }
function writing {
  /usr/bin/curl "http://$HOST:$PORT/nginx-status" 2>/dev/null| grep 'Writing' | awk '{print $4}'
  }
function waiting {
  /usr/bin/curl "http://$HOST:$PORT/nginx-status" 2>/dev/null| grep 'Waiting' | awk '{print $6}'
  }
function accepts {
  /usr/bin/curl "http://$HOST:$PORT/nginx-status" 2>/dev/null| awk NR==3 | awk '{print $1}'
  }
function handled {
  /usr/bin/curl "http://$HOST:$PORT/nginx-status" 2>/dev/null| awk NR==3 | awk '{print $2}'
  }
function requests {
  /usr/bin/curl "http://$HOST:$PORT/nginx-status" 2>/dev/null| awk NR==3 | awk '{print $3}'
  }

#formatsqlfile
echo $dataformaturl
if [[ $dataformat -eq 1 ]];then
	 echo "insert into mn_nginx_status values('$rid.nginx.${nginx_name}','`active`','`reading`','`writing`','`waiting`','`accepts`','`handled`','`requests`','$(date "+%Y%m%d:%H%M%S")',sysdate)">>$dataformaturl
   echo "--NEXT">>$dataformaturl
fi
######Exit############################################
unlock $lockurl
exit 0
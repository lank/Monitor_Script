#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#os_cmd.fun:DF
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
. ${scriptsdir}/lib/sysf/os_cmd.fun
#. ${scriptsdir}/lib/sysf/reclog.fun
#include Env
lockurl=${scriptsdir}/locks/os.df.lck
datafilelast=${scriptsdir}/data/os.fs.dat.last
#lock
if [[ `islock $lockurl` -eq 1 ]];then
	#reclog  "OS.FS" "ERROR:file is locked."
	exit 1
fi
lock $lockurl $$
######Main############################################
#data
line=1
($DF 2>/dev/null)|tr -d '%'|grep -v '/dev/sr0'|grep -v '/proc'|
while read f0 f1 f2 f3 f4 f5 f6
do
	#use to exclude the first line
	if [[ $line -eq 1 ]];then
		line=0
		continue;
	fi
	if [[ $f2 = "" ]]; then
		continue
	fi
	if [[ $f6 = "" ]]; then
		echo "$f0 $f1 $f2 $f3 $f4 $f5" >>$datafilelast
	else
		fno=`expr $f1 - $f2`
		echo "$f0 $f1 $fno $f2 $f3 $f6" >>$datafilelast
	fi
done
#formatsqlfile
if [[ $dataformat -eq 1 ]];then
	cat $datafilelast|while read f0 f1 f2 f3 f4 f5
	do
		echo "insert into mn_os_fs values('$rid','$f0','$f1','$f2','$f3','$f4','$f5','$(date "+%Y%m%d:%H%M%S")',to_date(to_char(sysdate,'yyyy-mm-dd hh24:mi'),'yyyy-mm-dd hh24:mi'))">>$dataformaturl
		echo "--NEXT">>$dataformaturl
	done
fi
######Exit############################################
rm -f $datafilelast>/dev/null
unlock $lockurl
exit 0
#!/bin/sh
######FUNC#############################################
dataupload()
{
reclog "MN" "Start check data."
errfile="${scriptsdir}/tmp/uploaderr.tmp"
>$errfile
if [[ `cat $dataformaturl|wc -l` -ge 2 ]];then
	reclog "MN" "Start upload."
	if [[ $dataformat -eq 1 ]];then
		#add heartbeat
		echo "insert into mn_node_heartbeat values('$rid','active',sysdate)">>$dataformaturl
		echo "--NEXT">>$dataformaturl
		#format sqlfile
		if [[ $uploadtype -eq 1 ]];then
			reclog "MN" "	uploadtype:java jar."
			#reclog "MN" "	java -jar $scriptsdir/lib/jlibf/sqlrun.jar rsql url=jdbc:oracle:thin:@$ip:$port:$sid user=$user secpwd=$secpwd sqlfile=$dataformaturl"
			(java -jar $scriptsdir/lib/jlibf/sqlrun.jar rsql url=jdbc:oracle:thin:@$ip:$port:$sid user=$user secpwd=$secpwd sqlfile=$dataformaturl) >$errfile 2>&1
			#runrst=`java -jar $scriptsdir/lib/jlibf/sqlrun.jar rsql url=jdbc:oracle:thin:@$ip:$port:$sid user=$user secpwd=$secpwd sqlfile=$dataformaturl 2>&1|grep SQLException|wc -l`
			runrst=$(grep -i -E "at|Exception|main" $errfile|wc -l)
			if [[ $runrst -ge 1 ]];then
				reclog "MN" "Error,$(cat $errfile)"
			fi
		elif [[ $uploadtype -eq 2 ]];then
			reclog "MN" "	Upload type:sqlplus."
			dosqlurl=$scriptsdir/tmp/do.sql
			sed "s/--NEXT/;/" $dataformaturl >$dosqlurl
			#notices:sqlnet.ora
			sqlplus $user/$pwd@$ip:$port/$sid >$errfile<<EOF
			@$dosqlurl
			commit;
			quit;
EOF
			runrst=$?
		elif [[ $uploadtype -eq 3 ]]; then
			#statements
			reclog "MN" "	uploadtype:ftp."
			ftpdataname="$rid.data.$(date "+%Y%m%d%H%M%S").sql"
			cat $dataformaturl>$scriptsdir/tmp/$ftpdataname
			ftplog=$scriptsdir/tmp/ftplog.dat
			ftp -in>$ftplog<<EOF
			open $agentftpip
			user $agentftpuser $agentftppwd
			binary
			prompt off
			cd $agentftpupload
			lcd $scriptsdir/tmp/
			put $ftpdataname
			bye
EOF
		runrst=`grep "Not connected" $ftplog|wc -l`
		if [[ $runrst -eq 0 ]];then
			runrst=`grep "refused" $ftplog|wc -l`
			if [[ $runrst -eq 0 ]];then
				runrst=`grep "incorrect" $ftplog|wc -l`
			fi
		fi
		rm -f $scriptsdir/tmp/$ftpdataname>/dev/null
		fi
	fi
	reclog "MN" "	Upload finished."
else
	reclog "MN" "	No data upload."
fi
>$dataformaturl
if [[ $runrst -ne 0 && $killself -eq 1 ]];then
	processtopcnt=$(($processtopcnt+1))
	reclog "MN" "ERROR:Upload count:$processtopcnt,max($errmaxnum)."
	if [[ $processtopcnt -gt $errmaxnum ]];then
		if [[ -f $mnpidurl ]];then
			rm -f $mnpidurl>/dev/null
		fi
		kill -9 $$
	fi
fi
}
#######Include conf######################################
scriptsdir=$(cd `dirname $0`; pwd)/..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/reclog.fun
. ${scriptsdir}/lib/sysf/lock.fun
#env
processtopcnt=0
debug=0
if [[ $logdebug -eq 1 ]];then
	debug=1
fi
if [[ -d "${scriptsdir}/locks" ]];then
	rm -rf *.lck
fi

######MAIN############################################### 
lock $mnpidurl $$
>$dataformaturl
###################### First run #########################
reclog "MN" "Start execute shells.[First]"
for run_shell in $shell_list
do
	TCNT=`echo $run_shell|awk -F ',' '{print $1}'`
	ns=`echo $TCNT|grep "#"|wc -l`
	if [[ $ns -eq 1 ]];then
		continue;
	fi
	CMD=`echo $run_shell|awk -F ',' '{print $2}'`
	PARS1=`echo $run_shell|awk -F ',' '{print $3}'`
	PARS2=`echo $run_shell|awk -F ',' '{print $4}'`
	PARS3=`echo $run_shell|awk -F ',' '{print $5}'`
	reclog "MN" "	execute[$scriptsdir/$CMD $PARS1 $PARS2 $PARS3 [First],finished exit=$?"
	$scriptsdir/$CMD $PARS1 $PARS2 $PARS3
done
reclog "MN" "Execute once shells finished.[First]"
reclog "MN" "-----------------------------------OVER."

###################### once do ############################
reclog "MN" "Start execute shells(once)."
for run_shell_once in $shell_once_list
do
	CMD=`echo $run_shell_once|awk -F ',' '{print $1}'`
	ns=`echo $CMD|grep "#"|wc -l`
	if [[ $ns -eq 1 ]];then
		continue;
	fi
	PARS1=`echo $run_shell_once|awk -F ',' '{print $2}'`
	PARS2=`echo $run_shell_once|awk -F ',' '{print $3}'`
	$scriptsdir/$CMD $PARS1 $PARS2
	reclog "MN" "	execute once[$scriptsdir/$CMD $PARS1 $PARS2],finished exit=$?"
done
dataupload
reclog "MN" "Execute shells(once) finished."

###################### Main while true ######################
arrarylen=`echo $shell_list|awk '{print NF}'`
j=1
while [[ $j -le $arrarylen ]]
do
	runstatus[$j]=1
	j=$(($j+1))
done

until test 0 -eq 1
do
	i=1
	reclog "MN" "Start execute shells."
	for run_shell in $shell_list
	do
		TCNT=`echo $run_shell|awk -F ',' '{print $1}'`
		ns=`echo $TCNT|grep "#"|wc -l`
		if [[ $ns -eq 1 ]];then
			continue;
		fi
		CMD=`echo $run_shell|awk -F ',' '{print $2}'`
		PARS1=`echo $run_shell|awk -F ',' '{print $3}'`
		PARS2=`echo $run_shell|awk -F ',' '{print $4}'`
		PARS3=`echo $run_shell|awk -F ',' '{print $5}'`
		memflag=${runstatus[$i]}
		if [[ "$memflag" -ge $TCNT ]];then		
			$scriptsdir/$CMD $PARS1 $PARS2 $PARS3
			if [[ $debug -eq 1 ]];then
				reclog "MN" "	execute[$scriptsdir/$CMD $PARS1 $PARS2 $PARS3],finished exit=$?"
			fi
			runstatus[$i]=1
		else
			if [[ $debug -eq 1 ]];then
				reclog "MN" "	info[${runstatus[$i]},$scriptsdir/$CMD $PARS1 $PARS2 $PARS3]"
			fi
			runstatus[$i]=$(($memflag+1))
		fi
		i=$(($i+1))
	done
	dataupload
	reclog "MN" "Execute shells finished."
	reclog "MN" "--------------------------------------------------------------------"
	sleep $sleeptime
done
reclog "N" "N"
######Exit#############################################
exit 0

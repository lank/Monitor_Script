#!/bin/sh
#########Func#########################################################
mnhelp()
{
mnbanner
cat <<EOF
Usage:
	./Mn.sh start
	./Mn.sh restart
	./Mn.sh stop
	./Mn.sh status
	./Mn.sh test
EOF
}

mninit()
{
#mn check dir 
#/tmp
if [ ! -d "$scriptsdir/tmp" ];then
	mkdir "$scriptsdir/tmp" 
	mkdir "$scriptsdir/tmp/ftpupload" 
else
	if [[ ! -d $scriptsdir/tmp/ftpupload ]];then
		mkdir "$scriptsdir/tmp/ftpupload" 
	fi
fi
#/data
if [ ! -d "$scriptsdir/data" ];then
	mkdir "$scriptsdir/data" 
	mkdir "$scriptsdir/data/archive" 
	mkdir "$scriptsdir/data/dbbackup" 
fi
#/locks
if [ ! -d "$scriptsdir/locks" ];then
	mkdir "$scriptsdir/locks"
fi

#check cmd
cmds='sar
vmstat
df
ifconfig
netstat
sed
'
for cmdstr in $cmds
do
	(which $cmdstr)>/dev/null 2>&1
	if [[ $? -gt 0 ]];then
		echo $cmdstr is not exists.
		reclog "MN-MAIN" "$cmdstr is not exists."
	fi
done
}

mnisrun()
{
#0 is not running
#1 is ok
#2 is runging,but configuration is err.
retrunflag=0
#reclog "MN-MAIN" "Call mnisrun."
mnpidin=$(ps -ef |grep "bin/Mnbase.sh"|grep -v grep|grep -v defunct|grep -v vim|grep -v vi|grep -v more|awk '{print $2}'|tail -1)
touch $mnpidurl
mnpidcnf=$(awk -F ',' '{print $2}' $mnpidurl)
#reclog "MN-MAIN" "Running Pid:$mnpidin."
#reclog "MN-MAIN" "Configuration Pid:$mnpidcnf." 
if [[ $(ps -ef |grep "bin/Mnbase.sh"|grep -v grep|grep -v defunct|grep -v vim|grep -v vi|grep -v more|awk '{print $2}'|wc -l) -gt 0 ]];then
	if [[ $mnpidin -eq $mnpidcnf ]];then
		retrunflag=1
		#reclog "MN-MAIN" "Mn is running normal."
	else
		#reclog "MN-MAIN" "Mn is running abnormal."
		retrunflag=2
	fi
else
	retrunflag=0
	#reclog "MN-MAIN" "Mn is not Running."
fi
echo "$retrunflag,$mnpidin,$mnpidcnf"
}

mnstart()
{
reclog "MN-MAIN" "Call mnstart."
if [[ $(mnisrun|awk -F ',' '{print $1}') -eq 1 ]];then
	reclog "MN-MAIN" "Mn is running normal,pid=$(mnisrun|awk -F ',' '{print $2}')"
	echo "Mn is running normal,pid=$(mnisrun|awk -F ',' '{print $2}')"
elif [[ $(mnisrun|awk -F ',' '{print $1}') -eq 2 ]];then
	reclog "MN-MAIN" "Mn is running abnormal,runpid=$(mnisrun|awk -F ',' '{print $2}'),cnfpid=$(mnisrun|awk -F ',' '{print $3}')"
	echo "Mn is running abnormal,runpid=$(mnisrun|awk -F ',' '{print $2}'),cnfpid=$(mnisrun|awk -F ',' '{print $3}')"
else
	reclog "MN-MAIN" "MN Start at background."
	echo "MN Start at background."
	nohup ${scriptsdir}/bin/Mnbase.sh>/dev/null &	
fi
}
 
mnstop()
{
reclog "MN-MAIN" "Call mnstop."
if [[ $(mnisrun|awk -F ',' '{print $1}') -eq 0 ]];then
	echo "Mn is not Running."	
else
	echo "Mn is Running."
	echo "Mn start to kill"
	kill -9 $(mnisrun|awk -F ',' '{print $2}')
	echo "0,0">$mnpidurl
	sleep 1
	if [[ $(mnisrun|awk -F ',' '{print $1}') -eq 0 ]];then
		echo "Mn is not Running."	
	else
		echo "Mn is Running."
	fi	
fi
}

mnstatus()
{
reclog "MN-MAIN" "Call mnstatus."
if [[ $(mnisrun|awk -F ',' '{print $1}') -eq 0 ]];then
	echo "Mn is not Running."	
else
	echo "Mn is running,runpid=$(mnisrun|awk -F ',' '{print $2}'),cnfpid=$(mnisrun|awk -F ',' '{print $3}')"
fi
}

mntest()
{
reclog "MN-MAIN" "Call mnteststart."
if [[ $(mnisrun|awk -F ',' '{print $1}') -eq 1 ]];then
	reclog "MN-MAIN" "Mn is running normal,pid=$(mnisrun|awk -F ',' '{print $2}')"
	echo "Mn is running normal,pid=$(mnisrun|awk -F ',' '{print $2}')"
elif [[ $(mnisrun|awk -F ',' '{print $1}') -eq 2 ]];then
	reclog "MN-MAIN" "Mn is running abnormal,runpid=$(mnisrun|awk -F ',' '{print $2}'),cnfpid=$(mnisrun|awk -F ',' '{print $3}')"
	echo "Mn is running abnormal,runpid=$(mnisrun|awk -F ',' '{print $2}'),cnfpid=$(mnisrun|awk -F ',' '{print $3}')"
else
	reclog "MN-MAIN" "MN Start at background."
	echo "MN Start at background."
	${scriptsdir}/bin/Mnbase.sh test
fi
}

mnmkcnf()
{
reclog "MN-MAIN" "Call mnmkcnf."
. ${scriptsdir}/lib/sysf/os_cmd.fun
if [[ -f "${scriptsdir}/conf/mn.cnf" ]];then
	cp "${scriptsdir}/conf/mn.cnf" "${scriptsdir}/conf/mn.cnf.$(date "+%Y%m%d%I%M%S")"
	if [[ $OS_TYPE = "LINUX" ]];then
		ridin=$(date|md5sum|awk '{print $1}')
	elif [[  $OS_TYPE = "AIX" ]];then
		ridin=$( ( date|csum |awk '{print $1}') 2>/dev/null )
	elif [[  $OS_TYPE = "HP" ]];then
		ridin=$(date|cksum|awk '{print $1 $2}')
	else
		ridin=$(date "+%Y%m%d")
	fi
	cat "${scriptsdir}/conf/mn.example.cnf" |grep -v "rid=" > "${scriptsdir}/conf/mn.cnf"
	echo "rid=$(hostname).$ridin">>"${scriptsdir}/conf/mn.cnf"
fi
}
 
#########Main#########################################################
scriptsdir=$(cd `dirname $0`; pwd)/..
. ${scriptsdir}/lib/sysf/reclog.fun
. ${scriptsdir}/lib/sysf/mn.fun
. ${scriptsdir}/conf/mn.cnf
mninit
if [ $1 ];then
	if [ $1 = "start" ];then
		mnstart
	elif [ $1 = "stop" ];then
		mnstop
	elif [ $1 = "status" ];then
		mnstatus
	elif [ $1 = "restart" ];then
		mnstop
		sleep 1
		mnstart
	elif [ $1 = "test" ];then
		mntest
	elif [ $1 = "mkcnf" ];then
		mnmkcnf
	else
		mnhelp
	fi
else
	mnhelp
fi
exit 0

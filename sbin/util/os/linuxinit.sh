#!/bin/sh
rlog()
{
# BLK='\E[1;30m'
# RED='\E[1;31m'
# GRN='\E[1;32m'
# YEL='\E[1;33m'
# BLU='\E[1;34m'
# MAG='\E[1;35m'
# CYN='\E[1;36m'
# WHI='\E[1;37m'
# DRED='\E[0;31m'
# DGRN='\E[0;32m'
# DYEL='\E[0;33m'
# DBLU='\E[0;34m'
# DMAG='\E[0;35m'
# DCYN='\E[0;36m'
# DWHI='\E[0;37m'
# RES='\E[0m'\
# echo -e " 01- ${BLK} BLK ${RES} "
# echo -e " 02- ${RED} RED ${RES} "
# echo -e " 03- ${GRN} GRN ${RES} "
# echo -e " 04- ${YEL} YEL ${RES} "
# echo -e " 05- ${BLU} BLU ${RES} "
# echo -e " 06- ${MAG} MAG ${RES} "
# echo -e " 07- ${CYN} CYN ${RES} "
# echo -e " 08- ${WHI} WHI ${RES} "
# echo -e " 09- ${DRED} DRED ${RES} "
# echo -e " 10- ${DGRN} DGRN ${RES} "
# echo -e " 11- ${DYEL} DYEL ${RES} "
# echo -e " 12- ${DBLU} DBLU ${RES} "
# echo -e " 13- ${DMAG} DMAG ${RES} "
# echo -e " 14- ${DCYN} DCYN ${RES} "
# echo -e " 15- ${DWHI} DWHI ${RES} "
BLK='\E[1;30m'
RED='\E[1;31m'
GRN='\E[1;32m'
YEL='\E[1;33m'
BLU='\E[1;34m'
MAG='\E[1;35m'
CYN='\E[1;36m'
WHI='\E[1;37m'
DRED='\E[0;31m'
DGRN='\E[0;32m'
DYEL='\E[0;33m'
DBLU='\E[0;34m'
DMAG='\E[0;35m'
DCYN='\E[0;36m'
DWHI='\E[0;37m'
RES='\E[0m'
COLOR='\E[0m'
case $1 in 
	BLK)
		COLOR=$BLK
		;;
	RED)
		COLOR=$RED
		;;
	GRN)
		COLOR=$GRN
		;;
	*)
		echo tcp_tw_recycle
		COLOR=$RES
esac
echo -e "${COLOR}${2}${RES}"
}

# openport=`ss -ln | tail --lines=+2 | awk '{print $3}' | egrep -v "^(127\.|::1|fe80)" | sed -e 's/:/ /g' | awk '{print $NF}' | tr '\n' , | sort -u | sed '$s/.$//'`
fnetpars()
{
echo "Network to adjust"
cat <<END_SCRIPT >> /etc/sysctl.conf

net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.rp_filter = 1
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.default.secure_redirects = 0

########################
##1是开启SYN Cookies，当出现SYN等待队列溢出时，启用Cookies来处，理，可防范少量SYN攻击，默认是0关闭
########################
net.ipv4.tcp_syncookies = 1

########################
##1是开启重用，允许讲TIME_AIT sockets重新用于新的TCP连接，默认是0关闭
########################
net.ipv4.tcp_tw_reuse = 1

########################
##TCP失败重传次数，默认是15，减少次数可释放内核资源 
########################
net.ipv4.tcp_tw_recycle = 1

########################
##应用程序可使用的端口范围
########################
net.ipv4.ip_local_port_range = 4096 65000

########################
#系统同时保持TIME_WAIT套接字的最大数量，如果超出这个数字，TIME_WATI套接字将立刻被清除并打印警告信息，默认180000
########################
net.ipv4.tcp_max_tw_buckets = 5000

########################
#进入SYN宝的最大请求队列，默认是1024
########################
net.ipv4.tcp_max_syn_backlog = 4096

########################
#允许送到队列的数据包最大设备队列，默认300
########################
net.core.netdev_max_backlog =  10240

########################
#listen挂起请求的最大数量，默认128
########################
net.core.somaxconn = 2048 


########################
#SYN-ACK握手状态重试次数，默认5 
########################
net.ipv4.tcp_synack_retries = 2 

########################
#向外SYN握手重试次数，默认4 
########################
net.ipv4.tcp_syn_retries = 2

########################
#开启TCP连接中TIME_WAIT sockets的快速回收，默认是0关闭 
########################
net.ipv4.tcp_tw_recycle = 1

END_SCRIPT
sysctl –p /etc/sysctl.conf


# Protect against taking part in an ICMP "Smurf"
echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts

# Protect against TCP Time-Wait attacks          
echo 1 > /proc/sys/net/ipv4/tcp_rfc1337

# Don't bother logging bogus error responses					 
echo 1 > /proc/sys/net/ipv4/icmp_ignore_bogus_error_responses

# Don't bother logging spoofed IP addresses
echo 0 > /proc/sys/net/ipv4/conf/all/log_martians

# Protect against Source Routed Packets                
echo 0 > /proc/sys/net/ipv4/conf/all/accept_source_route    
}

fulimitof()
{
	if [[ -f  "/etc/rc.local" ]];then
		rlog GRN "fulimitof|Check opefile"
		ofcnt=`grep "ulimit -SHn" /etc/rc.local|wc -l`
		if [[ $ofcnt -ge 1 ]];then
			rlog RED "  OK(HAD)"
		else
			rlog GRN "    ulimit -SHn 102400>>/etc/rc.local"
			echo "ulimit -SHn 102400" >>/etc/rc.local
			rlog RED "  OK."
		fi
			
	else
		rlog RED "  /etc/rc.local not exits."
	fi
}

fservicedisable()
{
	rlog GRN "fservicedisable|Check Service"
	cd /etc/rc.d/init.d/
	servicelist="
	apmd
	gpm
	innd
	irda
	isdn
	kdcrotate
	lvs
	mars-nwe
	oki4daemon
	rstatd
	rusersd
	rwalld
	rwhod
	identd
	ntpd
	rhnsd
	ipchains
	iptables
	ip6tables
	wine
	lpd
	pcmcia
	capi
	sendmail
	postfix
	nfs
	nfslock
	ypbind
	ypserv
	yppasswdd
	portmap
	smb
	netfs
	vsftpd
	snmpd
	named
	xfs
	canna
	FreeWnn
	kdcrotate
	cups
	atd
	auditd
	bluetooth
	saslauthd
	"
	for service in $servicelist
	do
		if [ -f $service ] ; then
			rlog GRN "  Process service $service"
			chkconfig --level 12345 $service off
			service $service stop
			rlog RED "  OK"
		fi
	done
}

fselinux()
{
	rlog GRN "fselinux|selinux disable"
	sed -i '/SELINUX/s/enforcing/disabled/' /etc/selinux/config
	setenforce 0 
	rlog RED "  OK"
}

fuserdel()
{
	rlog GRN "fuserdel|check user"
	userlist="
	adm
	lp
	shutdown
	halt
	uucp
	operator
	games
	gopher
	"
	for u in $userlist
	do
		rlog GRN "  userdel $u"
		userdel $u
		rlog RED "  OK"
	done
}

fosbanner()
{
	rlog GRN "fosbanner|check banner"
	if [[ -f "/etc/issue" ]];then
		rlog GRN "  check /etc/issue"
		echo "Welcome to Server" >/etc/issue 
		rlog RED "  OK"
	fi

	if [[ -f "/etc/redhat-release" ]];then
		rlog GRN "  check /etc/redhat-release"
		echo "Welcome to Server" >/etc/redhat-release
		rlog RED "  OK"
	fi
}

fcmdhistory()
{
	rlog GRN "fcmdhistory|check cmdhistory"
	if [[ -f "/etc/profile" ]];then
		fcnt=`cat /etc/profile|grep "export HISTFILESIZE=1"|wc -l`
		hcnt=`cat /etc/profile|grep "export HISTSIZE=10"|wc -l`
		if [[ $fcnt -ge 1 && $hcnt -ge 1 ]];then
			rlog RED "  OK(HAD)"
		else
			echo "export HISTFILESIZE=1 " >>/etc/profile
			echo "export HISTSIZE=10 " >> /etc/profile
			rlog RED "  OK"
		fi
	else
		rlog RED "  /etc/profile not exits."
	fi
}

 



--create user fk identified by oraclek
---------------MN CONF--------------------------------------------------
drop table mn_node_log;  
create table mn_node_log
(
node varchar2(500),
type varchar2(50), --host_linux|aix|hp , tms_bakup , xiv_log ...
mlevel number,--min_severity=HB(-1)|INFORMATION (0)|WARNING(1)|MINOR(2)|MAJOR(3)|CRITICAL(4)
message varchar2(1000),
mark varchar2(100),
inserttime date
)tablespace mn;
create index mnnodelogind1 on mn_node_log (node,mlevel,inserttime) tablespace mn;
create unique index mnnodelogind2 on mn_node_log(node,mlevel,message) tablespace mn;

--select sum(bytes)/1024/1024 size_m from dba_segments where  owner='MN';
--mn_node_heartbeat表用来确认node是否存活信息
--主机存活:直接发送
--Oracle数据库:监听打开且数据库读写模式
drop table mn_node_heartbeat;  
create table mn_node_heartbeat
(
node varchar2(500),
status varchar2(10), --active,inactive
updatetime date
)tablespace mn;
create index mnhbind2 on mn_node_heartbeat (node,updatetime) tablespace mn;
create or replace view mn_node_hb_view as select node,max(updatetime) lastdate from mn_node_heartbeat group by node; 
create or replace view mn_node_ping as
select node,
ceil( ( sysdate - lastdate ) * 24 * 60 ) time,
case when
ceil( ( sysdate - lastdate ) * 24 * 60 )  < 10   then 1  ELSE 0 END  status,lastdate inserttime
from mn_node_hb_view;

--MN的主要配置,根据nodedetail里的json字段配置,table为首列(必须)
--table:列出数据保存的表,'|'作为间隔,表中必须有inserttime字段,作为删除的判断
--dataetain=365天;table列车的表中的数据保留天数,如果是0则不删除数据
--analyze=on;分析功能开启on|off,将自动逐个分析table列出的表

drop table mn_node_info; --该表已经不使用
create table mn_node_info
(
node varchar2(500),
nodename varchar2(500), --hostname
agent varchar2(500),
nodetype varchar2(40), --host
nodedetail varchar2(500), --detail
status varchar2(100), --valid,invalid,unknow,Temporary
mark varchar2(100),
modifytime date,
createtime date,
inserttime date
)tablespace mn;
insert into mn_node_info values('MN','MNBASE','MN','MN','{"table":"mn_os_fs|mn_os_cpumem|mn_os_load","dataetain":"365","analyze":"on"}','valid','MN CONFIGURATION',sysdate,sysdate,sysdate);
create index mnnodeinfoind3 on mn_node_info (agent) tablespace mn;
create unique index mnnodeinfoind2 on mn_node_info (node) tablespace mn;

--在itemtype=NODE时
--item的字段有
--NAME:别名
--AGENT:这个node依赖于某个agent,上级为agent
--TYPE:
--NODETYPE:HOST(TYPE:AIX|Linux|HP|WIN2003)|DATABASE(TYPE:ORADB|MSSQL|MYSQL)|BACKUP(type:db|file)|APPS|OTHERS
--	   额外增加的字段HOST:ip,mem,cpu | DATABASE(ora):ip,lsnport | BACKUP:policy
--STATUS:VALID|INVALID|TEMP|
--MARK:注释
--MODIFYTIME
--CREATETIME
--添加node的时候必须带的字段
--INFO:NAME,NODETYPE,STATUS,AGENT,MARK,CREATETIME,MODIFYTIME
--REG:
drop table mn_node_conf;
create table mn_node_conf
(
node varchar2(500),
item varchar2(100),
itemvalueA varchar2(100),
itemvalueB varchar2(100),
itemvalueC varchar2(100),
itemvalueD varchar2(100),
itemvalueE varchar2(100),
itemvalueF varchar2(100),
itemvalueG varchar2(1000),
itemvalueH varchar2(1000),
itemtype varchar2(100),
itemsubtype varchar2(100),
inserttime date
)tablespace mn;
create unique index nodeconfind1 on mn_node_conf(node,item,itemsubtype);
create index nodeconfind2 on mn_node_conf(node,item,itemtype)tablespace mn;
create index nodeconfind5 on mn_node_conf(itemtype,node,item,itemvalueB,itemvalueC,itemvalueD,itemvalueE,itemvalueG,itemsubtype) tablespace mn;
alter table mn_node_conf storage(buffer_pool keep);

drop table mn_node_warnmetric;
create table mn_node_warnmetric
(
userid varchar2(100),
node varchar2(500),
metric varchar2(500),
status varchar2(100)
)tablespace mn;
select * from 
(select 
a.node,
a.item metric,
a.itemvaluea status,
a.itemvaluec level1,
a.itemvalued level2
from mn_node_conf a  where  itemtype='NODE' and itemsubtype='REG' and itemvalueb='METRIC REG')aa,mn_sys_warnmetric bb
where aa.node=bb.node(+);
--视图
--节点信息
create or replace view mn_node_view as
select distinct node node,
(select itemvalueA from mn_node_conf b where item='NAME'  and a.node=b.node and itemtype='NODE')name,
(select itemvalueA from mn_node_conf b where item='AGENT'  and a.node=b.node and itemtype='NODE' )agent,
(select itemvalueA from mn_node_conf b where item='NODETYPE'  and a.node=b.node and itemtype='NODE')nodetype,
(select itemvalueA from mn_node_conf b where item='STATUS'  and a.node=b.node and itemtype='NODE')status,
(select itemvalueA from mn_node_conf b where item='MARK'  and a.node=b.node and itemtype='NODE')mark,
(select itemvalueA from mn_node_conf b where item='MODIFYTIME'  and a.node=b.node and itemtype='NODE')modifytime,
(select itemvalueA from mn_node_conf b where item='CREATETIME'  and a.node=b.node and itemtype='NODE')createtime
 from mn_node_conf a where a.itemtype='NODE' and itemsubtype='INFO';
--服务视图
create or replace view mn_node_service_view as
select  a.item service,a.itemvaluea serviceid,b.node node ,
(select itemvalueA from mn_node_conf c where item='STATUS'  and c.node=b.node and itemtype='NODE')status
from 
(select * from mn_node_conf where node='MN' and itemvalueb='SERVICE REG' and itemtype='SYS' and itemsubtype='REG')a
, 
(select * from mn_node_conf where  itemvalueb='SERVICE REG' and itemtype='NODE' and itemsubtype='REG' ) b where a.itemvaluea=b.itemvaluea;



--度量视图
create or replace view mn_node_metric_view as
select 
a.node,
a.item metric,
a.itemvaluea status,
a.itemvaluec level1,
a.itemvalued level2
from mn_node_conf a  where  itemtype='NODE' and itemsubtype='REG' and itemvalueb='METRIC REG';
--mn_node_conf表初始化
insert into mn_node_conf (node,item,itemvalueg,itemtype,itemsubtype,inserttime) values('MN','RESTREE','','SYS','WEB',sysdate);
--默认服务组
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemtype,itemsubtype,inserttime) values('MN','默认组','0','SERVICE REG','SYS','REG',sysdate);
--度量默认设置

--服务相关操作
--添加服务组
--insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemtype,itemsubtype,inserttime) values('MN','SERVICE TEST','12','SERVICE REG','SYS','REG',sysdate);
--删除服务组
--先删除NODE中的服务信息,在删除服务组
--delete from mn_node_conf where  itemtype='NODE' and itemsubtype='REG' and itemvalueB='SERVICE REG' and item='DEFAULT SERVICE';
--delete from mn_node_conf where  itemtype='SYS' and itemsubtype='REG' and itemvalueB='SERVICE REG' and item='DEFAULT SERVICE';
--检查服务组
-- create or replace view mn_node_service_chk_view as
-- select item servicename,node,
-- (select itemvalueA from mn_node_conf b where item='NODETYPE'  and a.node=b.node and itemtype='NODE' )nodetype
--  from mn_node_conf a where itemtype='NODE' and itemsubtype='REG' and itemvalueB='SERVICE REG'
-- and item not in (select item from mn_node_conf t where node='MN' and itemvalueB='SERVICE REG' and itemtype='SYS' and itemsubtype='REG' );

--节点相关操作
--添加节点(HOST)
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','NAME','$rid','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','STATUS','VALID','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','NODETYPE','HOST','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','AGENT','$rid','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','MARK','','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','MODIFYTIME','','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','CREATETIME','','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemtype,itemsubtype,inserttime) values('$rid','默认组','0','SERVICE REG','NODE','REG',sysdate);
--if host
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','CPU','','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','IP','','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('$rid','MEM','','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('$rid','LOAD','VALID','METRIC REG','5','10','0','NODE','REG',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('$rid','CPU','VALID','METRIC REG','86','95','0','NODE','REG',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('$rid','MEM','VALID','METRIC REG','99','100','0','NODE','REG',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('$rid','FS','VALID','METRIC REG','95','98','0','NODE','REG',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('$rid','MEMSWAP','VALID','METRIC REG','80','200','0','NODE','REG',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('$rid','HOSTPING','VALID','METRIC REG','10','15','0','NODE','REG',sysdate);

--(ORADB)
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','NAME','POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','STATUS','VALID','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','NODETYPE','ORADB','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','AGENT','POSDEV.280ee9732711931627f73fd2e15e1969','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','MARK','pos数据库','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','MODIFYTIME','','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','CREATETIME','','NODE','INFO',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','默认组','0','SERVICE REG','NODE','REG',sysdate);

insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','ORADBCONNECT','VALID','METRIC REG','1000','2000','0','NODE','REG',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','ORATBS','VALID','METRIC REG','90','99','0','NODE','REG',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','ORADBSIZE','VALID','METRIC REG','99999','999999','0','NODE','REG',sysdate);
insert into mn_node_conf(node,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvalueg,itemtype,itemsubtype,inserttime) values('POSPRDA.bde40c063d15df489c925fdc1c719a2f.db.{pos}','ORDDBPCT','VALID','METRIC REG','100','200','0','NODE','REG',sysdate);


--job

--删除,更新及查询NODE操作,条件雷同
--delete from mn_node_conf where itemtype='NODE' and node='nodename'
--获得要发送的metric
--循环发送
select c.node,c.nodetype,b.metric,t.value,level1,inserttime
from (
select node,
inserttime,
os_load value,
row_number() over(partition by node order by inserttime desc) rid
from mn_os_load  where inserttime>sysdate-2/24) t,
mn_node_metric_view b,mn_node_view c
where rid=1  
and t.node=b.node
and metric='LOAD'
and b.node=c.node
and value<level2;

--备份检查昨天的
drop table mn_backup;
create table mn_backup
(
node varchar2(1000),
chkmethod varchar2(100), --TSM,ORAINTER
methoddtl varchar2(1000),
bktype varchar2(1000),--ORADB
stime date,
etime date,
elatime number,
sizeg number,
status varchar2(100), --'success','failed,'normal','unknow'
mark varchar2(4000),
inserttime date
)tablespace mn;
create index mnbackupind1 on mn_backup (node,inserttime,status) tablespace mn;

CREATE or replace TRIGGER bktrigger
AFTER
INSERT on mn_node_log
FOR EACH ROW 
DECLARE
status  VARCHAR2(30);
statustmp  VARCHAR2(30);
methoddtl varchar2(100);
begin
     if :new.type = 'backup_tsm' then
       statustmp:=trim(substr(:new.message,instr(:new.message,'状态:')+3));
       if statustmp = '已完成' then
         status :='success';
       elsif  statustmp = '未来的' then
         status:='future';
       else
         status:='falied';
       end if;
       methoddtl:=trim(substr(:new.message,instr(:new.message,'调度名称:')+5,instr(:new.message,' 节点名:')-instr(:new.message,'调度名称:') -4));
       INSERT INTO mn_backup(node,chkmethod,methoddtl,bktype,status,mark,inserttime) values('TSMSERVICE','TSM',methoddtl,'ORADB',status,:new.message ,:new.inserttime);
    end if;
end;
/

drop table mn_node_warn;
create table mn_node_warn
(
seq number not null,
status varchar2(100),
nodetype varchar2(400),
metrictype varchar2(400),
node varchar2(400),
nodename varchar2(400),
nodemark varchar2(40),
info varchar2(4000),
warnlevel varchar2(50),
inserttime varchar2(40)
)tablespace mn;
create index mnnodewarnind1 on mn_node_warn(node,seq,inserttime,nodetype,status)tablespace mn;
alter table mn_node_warn storage(buffer_pool keep);

drop table mn_node_warn_tmp;
create table mn_node_warn_tmp
(
nodetype varchar2(400),
metrictype varchar2(400),
node varchar2(400),
nodename varchar2(400),
nodemark varchar2(40),
info varchar2(4000),
inserttime varchar2(40)
)tablespace mn;
------------------------------------------------------------------------
-------------OS TAB-----------------------------------------------------
------------------------------------------------------------------------
drop table mn_os_netflow;
create table mn_os_netflow
(
node varchar2(500),
netinfo varchar2(100),
netin number,
netout number,
producetime varchar2(100),
inserttime date
)tablespace mn;
create index mnosnetflowind on mn_os_netflow (node,inserttime) tablespace mn;
 
drop table mn_os_fs;
create table mn_os_fs
(
node varchar2(500),
lvname varchar2(300),
fsize number,
used number,
avail number,
usdrat number,
mountpoint varchar2(100),
producetime varchar2(100),
inserttime date
)tablespace mn;
create index monosfsndx4 on mn_os_fs(node,inserttime)tablespace mn;
create index monosfsndx2 on mn_os_fs(producetime)tablespace mn;
create index monosfsndx3 on mn_os_fs(inserttime)tablespace mn;
create or replace view mn_os_fs_warn_view as
select a.node,to_date(to_char(a.inserttime,'yyyy-mm-dd hh24:mi'),'yyyy-mm-dd hh24:mi') inserttime,a.usdrat,b.mountpoint  from (
select node,inserttime,max(usdrat) usdrat  from MN_OS_FS  group by node,inserttime) a,
MN_OS_FS b where a.node=b.node and a.inserttime=b.inserttime and a.usdrat=b.usdrat
and a.inserttime>sysdate-7
order by a.inserttime desc;

drop table mn_os_cpumem;
create table mn_os_cpumem
(
node varchar2(500),
runprocess number,
os_user number,
os_sys number,
os_idle number,
os_wio number,
os_mem_free number,
os_page_in number,
os_page_out number,
os_page_fr number,
os_page_sr number,
producetime varchar2(100),
inserttime date
)tablespace mn;
create index monoscmndx4 on mn_os_cpumem(node,inserttime) tablespace mn;
create index monoscmndx2 on mn_os_cpumem(producetime)tablespace mn;
create index monoscmndx3 on mn_os_cpumem(inserttime)tablespace mn;
create or replace view mn_os_cpu 
as select node,os_user,os_sys,os_idle,os_wio,producetime,inserttime from mn_os_cpumem;
create or replace view mn_os_paging
as select node,os_page_in,os_page_out,os_page_fr,os_page_sr,producetime,inserttime from mn_os_cpumem;

drop table mn_os_load;
create table mn_os_load
(
node varchar2(500),
users number,
proc number,
ora_proc number,
ora_sys_proc number,
os_load number,
producetime varchar2(100),
inserttime date
)tablespace mn;
create index monosldndx4 on mn_os_load(node,inserttime) tablespace mn;
create index monosldndx2 on mn_os_load(producetime)tablespace mn;
create index monosldndx3 on mn_os_load(inserttime)tablespace mn;


------------------------------------------------------------------------
-------------DB ORADB---------------------------------------------------
------------------------------------------------------------------------
drop table mn_oradb_redosize;
create table mn_oradb_redosize
(
node varchar2(500),
instance varchar2(10),
rectime date,
value number,
inserttime date
)tablespace mn;
create index mnoradbredosize1 on mn_oradb_redosize(node,inserttime)tablespace mn;

drop table mn_oradb_logicalreads;
create table mn_oradb_logicalreads
(
node varchar2(500),
instance varchar2(10),
rectime date,
value number,
inserttime date
)tablespace mn;
create index mnoradblogicalreads on mn_oradb_logicalreads(node,inserttime)tablespace mn;

drop table mn_oradb_eventdbtimepct;
create table mn_oradb_eventdbtimepct
(
node varchar2(500),
instance varchar2(10),
dbtimepct number,
dbrectime date,
producetime varchar2(100),
inserttime date
)tablespace mn;
create index mnoradbeventdbtimepct on mn_oradb_eventdbtimepct(node,inserttime)tablespace mn;

drop table mn_oradb_archsw;
create table mn_oradb_archsw
(
node varchar2(500),
oraname VARCHAR2(80),
seqnum number,
fre_pers number,
size_Kb number,
ftime date,
producetime varchar2(100),
inserttime date
)tablespace mn;
create index mndbarchswind4 on mn_oradb_archsw(node,inserttime)tablespace mn;
create index mndbarchswind2 on mn_oradb_archsw(producetime)tablespace mn;
create index mndbarchswind3 on mn_oradb_archsw(inserttime)tablespace mn;

--base table
create or replace view mn_oradb_archswview as
select * from mn_oradb_archsw where  (node,oraname,seqnum,inserttime) in
(select node,oraname,seqnum,min(inserttime) inserttime from mn_oradb_archsw group by node,oraname,seqnum);

----
----最近三个月每秒日志产生频率sec_per(Kb) , 每天日志产生频率day_per(Kb)
----
create or replace view mn_oradb_arch_avgfre_90 as
select node,oraname, trunc(sum(size_kb) / ((max(ftime) - min(ftime)) * 24 * 3600)) sec_per,
       trunc(sum(size_kb) / ((max(ftime) - min(ftime)))) day_per
from mn_oradb_archswview
where ftime > sysdate - 91 group by node,oraname;

--最近一个月归档日志每日产生量如下
create or replace view mn_oradb_arch_day_30 as
select node,
       oraname,
       LPAD(to_char(ftime, 'yyyymmdd'), 12) time,
       sum(size_kb) size_kb,
       count(1) arch_cnt
from mn_oradb_archswview
where ftime > sysdate - 31
group by node, oraname, LPAD(to_char(ftime, 'yyyymmdd'), 12)
order by 1, 2, 3;

----
----最近一周归档日志24小时分时产生量如下
----
create or replace view mn_oradb_arch_hour_7 as
select node,
       oraname,
       LPAD(to_char(ftime, 'hh24'), 4) hour,
       sum(size_kb) size_kb,
       count(1) arch_cnt
  from mn_oradb_archswview
 where ftime > sysdate - 7
 group by node, oraname, to_char(ftime, 'hh24')
 order by 1, 2, 3;


drop table mn_oradb_size;
create table mn_oradb_size
(
node varchar2(500),
oraname VARCHAR2(80),
allocate number,
free number,
producetime varchar2(100),
inserttime date
)tablespace mn;
create index monoradbszndx4 on mn_oradb_size(node,inserttime)tablespace mn;
create index monoradbszndx2 on mn_oradb_size(producetime)tablespace mn;
create index monoradbszndx3 on mn_oradb_size(inserttime)tablespace mn;

drop table mn_oradb_tbs;
create table mn_oradb_tbs
(
node varchar2(500),
oraname VARCHAR2(80),
tbsname varchar2(30),
sum_space number,
sum_blocks number,
used_space number,
used_rate number,
free_space number,
max_free_space number,
producetime varchar2(100),
inserttime date
)tablespace mn;
create or replace view mn_oradb_tbs_warn_view as
select node,inserttime,max(used_rate) used_rate from mn_oradb_tbs  group by node,inserttime;

create index monoradbtbsndx1 on mn_oradb_tbs(node,inserttime) tablespace mn;
create index monoradbtbsndx2 on mn_oradb_tbs(producetime)tablespace mn;
create index monoradbtbsndx3 on mn_oradb_tbs(inserttime)tablespace mn;
create index monoradbtbsndx4 on mn_oradb_tbs(node||'_'||oraname,inserttime);

drop table mn_oradb_session;
create table mn_oradb_session
(
node varchar2(500),
oraname VARCHAR2(80),
sesscnt number,
activesesscnt number,
producetime varchar2(100),
inserttime date
)tablespace mn;
create index monoradbsssndx1 on mn_oradb_session(node,inserttime) tablespace mn;
create index monoradbsssndx2 on mn_oradb_session(producetime)tablespace mn;
create index monoradbsssndx3 on mn_oradb_session(inserttime)tablespace mn;

drop table mn_oradb_ratio;
create table mn_oradb_ratio
(
node varchar2(500),
oraname VARCHAR2(80),
rationame varchar2(100),
value number,
producetime varchar2(100),
inserttime date
)tablespace mn;
create index monoradbrasndx1 on mn_oradb_ratio(node,inserttime) tablespace mn;
create index monoradbrasndx2 on mn_oradb_ratio(producetime)tablespace mn;
create index monoradbrasndx3 on mn_oradb_ratio(inserttime)tablespace mn;

drop table mn_oradb_lock;
create table mn_oradb_lock
(
node varchar2(500),
oraname VARCHAR2(80),
STATUS VARCHAR2(100),
WAITING_SESSION VARCHAR2(30),
USERNAME VARCHAR2(100),
OBJECT_NAME VARCHAR2(100),
OBJECT_ID VARCHAR2(30),
OWNER VARCHAR2(30),
OBJECT_TYPE VARCHAR2(100),
COMMAND VARCHAR2(100),
SID VARCHAR2(30),
producetime varchar2(100),
inserttime date
)tablespace mn;
create index monoradblksndx1 on mn_oradb_lock(node,inserttime) tablespace mn;
create index monoradblksndx2 on mn_oradb_lock(producetime)tablespace mn;
create index monoradblksndx3 on mn_oradb_lock(inserttime)tablespace mn;


------------------------------------------------------------------------
-------------others-----------------------------------------------------
------------------------------------------------------------------------

--------------------increase obj---------------------------------------
drop table mon$segment_space ;
create table  mon$segment_space 
(
 DB_NAME                                            VARCHAR2(300),
 LOG_DATE                                           DATE,
 OWNER                                              VARCHAR2(32),
 TABLE_NAME                                         VARCHAR2(32),
 TABLESPACE_NAME                                    VARCHAR2(50),
 EXTENTS                                            NUMBER,
 size_m                                            NUMBER);
 
drop table mon$obj_incr_per_h ;
create table mon$obj_incr_per_h 
(
 DB_NAME                                            VARCHAR2(300),
 TABLESPACE_NAME                                    VARCHAR2(50),
 owner                                              VARCHAR2(50),
 TABLE_NAME                                         VARCHAR2(32),
 SIZE_M                                             NUMBER,
 INCR_SIZE                                          NUMBER,
 log_DATE                                           DATE);

drop table mon$obj_incr_per_tbs_hour;
create table mon$obj_incr_per_tbs_hour 
 (DB_NAME                                            VARCHAR2(300),
 TABLESPACE_NAME                                    VARCHAR2(50),
  owner                                              VARCHAR2(50),
 TABLE_NAME                                         VARCHAR2(32),
 SIZE_M                                             NUMBER,
 INCR_SIZE                                          NUMBER,
 LOG_DATE                                           DATE,
 XH                                                 NUMBER);

create index  MON$SEGMENT_SPACE_ind1 on  MON$SEGMENT_SPACE(log_date);
create index  MON$SEGMENT_SPACE_ind2 on  MON$SEGMENT_SPACE(db_name);

create index  mon$obj_incr_per_h_ind1 on  mon$obj_incr_per_h(log_date);
create index  mon$obj_incr_per_h_ind2 on  mon$obj_incr_per_h(db_name);
create index obj_incr_per_tbs_hour_ind1 on  mon$obj_incr_per_tbs_hour(log_date);
create index obj_incr_per_tbs_hourh_ind3 on mon$obj_incr_per_tbs_hour(db_name);
alter table mon$obj_incr_per_h   add constraints  obj_incr_per_h_p  primary key (db_name,tablespace_name,owner,table_name,log_date);
alter table  mon$obj_incr_per_tbs_hour   add constraints obj_incr_per_tbs_hour_p  primary key (db_name,tablespace_name,owner,table_name,log_date);

------------------------------------------------------------------------------------


--------------------------------数据库性能------------------------------------------
--记录事件
drop table mon$db_perf_event;
create table mon$db_perf_event
(instance_name varchar(36),
log_date date,
event  varchar2(200),
event_count number,
partition_id number
);

--记录性能数据
drop table mon$db_perf_record;   
create table mon$db_perf_record    
(
instance_name varchar(36),
log_date date,
perf_pct number,
event_cnt_per_sec number,
now_day number
);
           
--最终告警
drop table mon$db_perf_warn	;
create table mon$db_perf_warn	
 (	log_date date, 
log_db char(6), 
log_option varchar2(100), 
full_log   varchar2(300), 
sms char(1) 
 ); 
-------------------------------------------------------------------------------------- 
 
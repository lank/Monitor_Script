ftpload()
{
#How to use 
#	ftpload  oracle oracle 192.168.196.85 /ftp/backup/gc 1 . "filename1 filename2 filename3" ones
#	ftpload  oracle oracle 192.168.196.85 /ftp/backup/gc 0 /data "filename1" one
#	ftpload  oracle oracle 192.168.196.85 /ftp/backup/gc 0 /data "file*.zip" muti  
#文件名不能用绝对路径
#remotedircr:到达远程目录后,是否建立以时间为名的子目录,1则建立,0不建立
#type:
#		one
#		ones
#		muti
if [[ -n $8 && ( $8 = "one"  || $8 = "ones" || $8 = "muti" ) ]];then
	echo can rune
else
	echo not run
	return 0
fi

inuser=$1
inpwd=$2
remotehost=$3
remotedir=$4
remotedircr=$5
localdir=$6
file=$7
type=$8

ftptmpfile="/tmp/ftptmp.log"
ftpcnffile="/tmp/ftpcnf.$$.cnf"
today=$(date "+%Y%m%d")
>$ftpcnffile
#make conf file
echo "open $remotehost" 		>> $ftpcnffile
echo "user $inuser $inpwd" 	    >> $ftpcnffile
echo "bin" 						>> $ftpcnffile
echo "lcd $localdir"  	     	>> $ftpcnffile
echo "cd $remotedir"            >> $ftpcnffile
if [[ $remotedircr -eq 1 ]];then
	echo "mkdir $today"         >> $ftpcnffile
	echo "cd $today"            >> $ftpcnffile
fi
if [[ $type = "one" ]];then
	echo "put $file"            >> $ftpcnffile
elif [[ $type = "ones" ]];then
	for ftpitem in $(echo $file)
	do
		echo "put $ftpitem"       >> $ftpcnffile
	done
else

	#echo "prompt off"            >> $ftpcnffile
	echo "mput $file"            >> $ftpcnffile
fi
echo "bye"          		    >> $ftpcnffile
ftp -in  <<EOF
$(cat $ftpcnffile)
EOF
}

mnbanner()
{
cat <<EOF
|----------------------------------------------------------------------------|
|      Copyright (c) 1998-2015 Hello Lank. All rights reserved.              |
|----------------------------------------------------------------------------|
|Info:                                                                       |
|     Thank You to use these scripts for Efficient work.                     |
|Notes:                                                                      |
|     As with any code, ensure to test this script in a development          |
|     environment before attempting to run it in production.                 |
|                                                                            |
|                                                                            |
|                                                         by Lank            |
|                                                         maillank@qq.com    |
|                                                         2014-04-05         |
|----------------------------------------------------------------------------|
EOF
}